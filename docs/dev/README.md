# Notice

Please take note that these guides are meant for developers.

## List of guides

- [`ActionIconService`](./actionicon-service.md)
- [Coding standards](./coding-standards.md)
- [Contributing](./contributing.md)
- [`OverlayService`](./overlay-service.md)
- [`SharedService`](./shared-service.md)
