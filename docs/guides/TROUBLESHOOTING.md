# Troubleshooting

The following guide below shows the typical errors and what you can do to solve it.

## RSS doesn't load

If the RSS feed app is stuck at loading, try reloading it again by clicking on the options button (shown below in the GIF).

![Options](../img/troubleshooting/rss_feed_not_loading.gif)

Alternatively, the RSS feed URL you have entered is invalid or doesn't exist. In this case, just reselect the RSS feed by clicking on the options button and selecting `Reselect RSS feed`.

## Issue isn't listed

In that case, just submit an issue [here][issue_form] and I will try to fix the problem.

[issue_form]: https://goo.gl/forms/hopvyFXOqHfpA1kq2

<!-- begin end links -->

---
[:arrow_left: Back to website](https://chan4077.github.io/rss-reader)

- [Generating an API Key](./generate-api-key.md)
- [Getting Started](./getting-started.md)
- [Troubleshooting **(You are here)**](./troubleshooting.md)

<!-- end links -->
