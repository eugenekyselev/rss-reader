# Angular RSS Reader

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version `1.2.5`.

# Serving locally
## Developer mode
Run `ng serve` and go to `http://localhost:4000`.
## Service worker (locally)
To enable the service worker locally, run the following commands on the app's root directory:
```bash
npm run build-prod-local
```
Go to `http://127.0.0.1:8080` to see it in action.


# Generating an API key

As the RSS dialog requires you to enter an API key, this guide will show you how to generate one.

**UPDATE:** I am currently planning to provide the API key to the `FeedDialog`. The following instructions below will soon be outdated.

---

Generate API Key on
[https://rss2json.com](https://rss2json.com)